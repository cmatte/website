[[!meta title="Minutes from previous board meetings"]]
[[!meta copyright="Copyright © 2010-2024 Software in the Public Interest, Inc."]]
[[!meta license="Creative Commons Attribution-ShareAlike 3.0 Unported"]]

# Minutes from previous board meetings

These are the minutes of recent SPI board meetings:

* [Monday, 12th February, 2024](2024/2024-02-12)
* [Monday, 8th January, 2024](2024/2024-01-08)
* [Monday, 11th December, 2023](2023/2023-12-11)
* [Monday, 13th November, 2023](2023/2023-11-13)

Minutes by year:

[[!inline pages="meetings/minutes/* and ! meetings/minutes/*/*" sort="-title" template=unordered-list-title archive=yes]]
